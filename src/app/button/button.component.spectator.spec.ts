import { ButtonComponent } from './button.component';
import { Spectator, createComponentFactory } from '@ngneat/spectator';

describe('Button Component - Spectator tests', () => {
  let spectator: Spectator<ButtonComponent>;
  const createComponent = createComponentFactory(ButtonComponent);

  beforeEach(() => (spectator = createComponent({})));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });

  it('should have a success class by default', () => {
    expect(spectator.query('button')).toHaveClass('success');
  });

  it('should set the class name according to the [className] input', () => {
    spectator.setInput('className', 'danger');
    expect(spectator.query('button')).toHaveClass('danger');
    expect(spectator.query('button')).not.toHaveClass('success');
  });

  it('should set the title according to the [title] input', () => {
    spectator = createComponent({
      props: {
        title: 'Click',
      },
    });
    expect(spectator.query('button')).toHaveText('Click');
  });

  it('should emit the updated form values when submitting a valid form', () => {
    spyOn(spectator.component.btnClick, 'emit');
    const event = 'test';
    spectator.component.onClick(event);
    expect(spectator.component.btnClick.emit).toHaveBeenCalledWith(event);
  });
});
