import { ButtonComponent } from './button.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Page } from '../test-helpers/page';
import { forceDetectChanges } from '../test-helpers/force-detect-changes';

describe('Button Component', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let page: Page<ButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonComponent],
      imports: [],
    })
      .overrideComponent(ButtonComponent, {
        set: { host: { '(click)': 'dummy' } },
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    page = new Page(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a success class by default', () => {
    expect(
      page.queryByTestAttribute('btn').nativeElement.getAttribute('class')
    ).toContain('success');
  });

  it('should set the class name according to the [className] input', () => {
    component.className = 'danger';

    forceDetectChanges(fixture);

    const classes = page
      .queryByTestAttribute('btn')
      .nativeElement.getAttribute('class');

    expect(classes).toContain('danger');
    expect(classes).not.toContain('success');
  });

  it('should set the title according to the [title] input', () => {
    component.title = 'Click';
    forceDetectChanges(fixture);

    const btn = page.queryByTestAttribute('btn');
    expect(btn.nativeElement.textContent).toEqual('Click');
  });

  it('should emit the updated form values when submitting a valid form', () => {
    spyOn(component.btnClick, 'emit');
    const event = 'test';
    component.onClick(event);
    expect(component.btnClick.emit).toHaveBeenCalledWith(event);
  });
});
