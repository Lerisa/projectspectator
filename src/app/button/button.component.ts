import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @Input() className = 'success';
  @Input() title = '';
  @Output() btnClick = new EventEmitter();

  onClick($event: any) {
    this.btnClick.emit($event);
  }
}
