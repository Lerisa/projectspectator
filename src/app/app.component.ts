import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project';
  className = 'btn btn-primary';
  btnEvent: any;

  btnClick(event: any) {
    this.btnEvent = event;
    console.log(event);
  }
}
